from django.shortcuts import render
from .models import Matches, Deliveries
from django.http import JsonResponse
from django.db.models import *
from django.db.models.functions import *


def api_matches_per_year(request):
    matches_per_year = {}
    matches_per_year_object = Matches.objects.values('season').annotate(Count('season')).order_by('season')
    matches_per_year['data'] = list(matches_per_year_object)
    return JsonResponse(matches_per_year)

def api_matches_won_per_year(request):
    matches_won_per_year = {}
    matches_won_per_year_object = Matches.objects.values('season', 'winner').annotate(no_of_wins = Count('winner')).order_by('season','winner')
    matches_won_per_year['data'] = list(matches_won_per_year_object)
    return JsonResponse(matches_won_per_year)

def top_economical_bowlers(request):
    economical_bowlers = {}
    economical_bowlers['data'] = list(Deliveries.objects.filter(match_id__season = 2015, is_super_over=False).values('bowler').annotate(runs=Sum('batsman_runs')+Sum('wide_runs')+Sum('noball_runs')).annotate(balls=Count('ball')-Count(Case(When(noball_runs__gt=0, then=1)))-Count(Case(When(wide_runs__gt=0, then=1)))).annotate(economy=Cast((F('runs')/(F('balls')/6.0)),FloatField())).order_by('economy')[:10])
    return JsonResponse(economical_bowlers)

    
    
    
    
    
    
# # City.objects.values('country__name') \
# # .annotate(country_population=Sum('population')) \
# # .order_by('-country_population')

# #=========================================

# # data = Deliveries.objects.values('bowler').annotate(economy = (  ( Sum('batsman_runs')+Sum('wide_runs')+Sum('noball_runs') )*6)/ Count('match_id'))
#     pass



# # SELECT bowling_team, SUM(extra_runs) as extra_runs FROM matches
# # INNER JOIN deliveries
# # ON matches.id = deliveries.match_id
# # WHERE season ='2016'
# # GROUP BY bowling_team;
# def extra_runs_conceded(request):
#     pass

# def top_scorer(request):
#     pass

##########################################################################
def matches_per_year(request):
    return render(request,'iplapp/matches_per_year.html')

def matches_won_per_year(request):
    return render(request,'iplapp/matches_won_per_year.html')


  
    
