# Generated by Django 2.2.6 on 2019-10-07 08:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('iplapp', '0003_auto_20191007_0732'),
    ]

    operations = [
        migrations.AlterField(
            model_name='deliveries',
            name='inning',
            field=models.IntegerField(blank=True, null=True),
        ),
    ]
