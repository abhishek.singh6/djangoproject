# The closepoll.py module has only one requirement – it must define a class Command that extends BaseCommand or one of its subclasses.
from csv import DictReader
from django.core.management.base import BaseCommand
from iplapp.models import Matches, Deliveries

class Command(BaseCommand):
    help = 'import the data from csv file to database'

    def handle(self, *args, **options):
        file_path = '/home/abhishek/Desktop/workof/dataproject/iplapp/management/commands/_matches.csv'
        Matches.objects.from_csv(file_path)
        self.stdout.write(self.style.SUCCESS('Successfully imported csv data to database'))
   

        
        
        
        # count = 0

        # with open(file_path, 'rt') as csv_file:
        #     matches = DictReader(csv_file)
        #     for match in matches:
        #         q = Matches(
        #             id = match['id'],
        #             season = match['season'],
        #             city = match['city'],
        #             date = match['date'],
        #             team1 = match['team1'],
        #             team2 = match['team2'],
        #             toss_winner = match['toss_winner'],
        #             toss_decision = match['toss_decision'],
        #             dl_applied = match['dl_applied'],
        #             winner = match['winner'],
        #             win_by_runs = match['win_by_runs'],
        #             win_by_wickets = match['win_by_wickets'],
        #             player_of_match = match['player_of_match'],
        #             venue = match['venue'],
        #             umpire1 = match['umpire1'],
        #             umpire2 = match['umpire2'],
        #             umpire3 = match['umpire3']
        #             )
        #         q.save()    
        #         count +=1
        #         print(count)