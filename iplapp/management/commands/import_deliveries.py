# The closepoll.py module has only one requirement – it must define a class Command that extends BaseCommand or one of its subclasses.
from csv import DictReader

from django.core.management.base import BaseCommand, CommandError
from iplapp.models import Matches, Deliveries

class Command(BaseCommand):
    help = 'import the data from csv file to database'

    def handle(self, *args, **options):
        file_path = '/home/abhishek/Desktop/workof/djangoproject/iplapp/management/commands/deliveries.csv'
        
        Deliveries.objects.from_csv(file_path)
    
        self.stdout.write(self.style.SUCCESS('Successfully imported delivery csv data to database'))
        
# with open(file_path, 'rt') as csv_file:
        #     deliveries = DictReader(csv_file)
        #     Matchess = Matches.objects.all().order_by('id')
        #     for match in Matchess:
        #         for delivery in deliveries:
        #             if str(match.id) == str(delivery['match_id']):
        #                 print(match.season)
        #                 m = Matches.objects.get(id = match.id)
        #                 d = m.deliveries_set.create(
        #                     match_id = delivery['match_id'],
        #                     inning = delivery['inning'],
        #                     batting_team = delivery['batting_team'],
        #                     bowling_team = delivery['bowling_team'],
        #                     over = delivery['over'],
        #                     ball = delivery['ball'],
        #                     batsman = delivery['batsman'],
        #                     non_striker = delivery['non_striker'],
        #                     bowler = delivery['bowler'],
        #                     is_super_over = delivery['is_super_over'],
        #                     wide_runs = delivery['wide_runs'],
        #                     bye_runs = delivery['bye_runs'],
        #                     legbye_runs = delivery['legbye_runs'],
        #                     noball_runs = delivery['noball_runs'],
        #                     penalty_runs = delivery['penalty_runs'],
        #                     batsman_runs = delivery['batsman_runs'],
        #                     extra_runs = delivery['extra_runs'],
        #                     total_runs = delivery['total_runs'],
        #                     player_dismissed = delivery['player_dismissed'],
        #                     dismissal_kind = delivery['dismissal_kind'],
        #                     fielder = delivery['fielder']
        #                 )
        #                 d.save()           