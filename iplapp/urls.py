from django.urls import path

from . import views

urlpatterns = [
    
# API URL    

    

    path('api/v1/matches_per_year', views.api_matches_per_year, name='api_matches_per_year'),
    path('api/v1/matches_won_per_year', views.api_matches_won_per_year, name='api_matches_won_per_year'),
    path('api/v1/top_economical_bowlers', views.top_economical_bowlers, name='api_top_economical_bowlers'),
    
    # path('extra_runs_conceded', views.extra_runs_conceded, name='extra_runs_conceded'),
    
    # path('top_scorer', views.top_scorer, name='top_scorer'),
    
    
# TEMPLATE URL    
    path('matches_per_year', views.matches_per_year, name='matches_per_year'),
    # path('matches_won_per_year', views.matches_won_per_year, name='matches_won_per_year'),
    
    # path('top_economical_bowlers', views.top_economical_bowlers, name='top_economical_bowlers'),
    # path('extra_runs_conceded', views.extra_runs_conceded, name='extra_runs_conceded'),
    # path('top_scorer', views.top_scorer, name='top_scorer'),
]
  